---
bookFlatSection: true
bookCollapseSection: true
title: "Mirto tarentina"
---

# Mirto tarentina

## Ficha

- Familia: **Myrtaceae**
- Genero: **Myrtus**
- Especie: **Myrtus communis**
- Subspecie: **Tarentina**

## Inicios

- **Procedencia**: Procede de la rama de un arbol de 28 años. Comprado en Bonsai Groves.
- **Fecha**: 24/09/2019
- **Sustrato**: Akadama, Pomice y algo de Kiryu
- **Transplante**: Previsto para primavera 2021
- **Abono**: Cada semana, abono lombrico y a-micsur, radicular y foliar

## Log

- [\[2019-09-07\] Compra]({{< relref "/docs/tarentina/2019-09-24-compra.md" >}})
- [\[2019-09-20\] Primera clase]({{< relref "/docs/tarentina/2019-10-18-primera-clase.md" >}})
- \[2019-09-20\] Abonado de otoño con lombrico
- [\[2019-11-20\] Desalambrado]({{< relref "/docs/tarentina/2019-11-20-desalambrado.md" >}})

## Calendario