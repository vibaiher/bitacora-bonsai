---
title: Inicio
type: docs
---

# Bitácora bonsai

Querida bitácora, hoy a sido un día genial... pero no he venido a contarte eso. Esta es mi bitácora sobre los trabajos que voy haciendo en mis bonsai.

## Bonsais

- [Mirto tarentina]({{< relref "/docs/tarentina/_index.md" >}})
- [Ficus nerifolia]({{< relref "/docs/nerifolia/_index.md" >}})
- [Citrus Kinzu]({{< relref "/docs/naranjo/_index.md" >}})
- [Buganvilla]({{< relref "/docs/buganvilla/_index.md" >}})